﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Comun;
using BRL;



namespace PresentacionWPF.Paginas
{
    /// <summary>
    /// Lógica de interacción para PgVentas.xaml
    /// </summary>
    public partial class PgVentas : Page
    {
        Venta venta = new Venta();
        List<Venta> ventas = new List<Venta>();
        DetalleVenta dv = new DetalleVenta();
        public PgVentas()
        {
            InitializeComponent();
            this.Height = Operaciones.alto;
            this.Width = Operaciones.ancho;
            cargardatos();
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PgNuevaVenta());
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (dtgVentas.SelectedIndex != -1) 
                NavigationService.Navigate(new PgEditarVentas());
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if(dtgVentas.SelectedIndex != -1) { }
                //NavigationService.Navigate(new PgEditarVentas());
        }

        private void cargardatos()
        {
            decimal sumTotal =0;
            ventas.Clear();
            ventas = VentasBrl.select();
            dtgVentas.ItemsSource = null;
            dtgVentas.ItemsSource = ventas;
            for (int i = 0; i < ventas.Count; i++)
            {
                sumTotal += ventas[i].TotalVenta;
            }
            txtSumTotal.Text=sumTotal+"";

        }
        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtgVentas.SelectedIndex != -1)
            {
                venta = dtgVentas.SelectedItem as Venta;
                VentasBrl.ventaSeleccionado = venta.VentaAux;
                VentasBrl.RazonSocial = venta.ClienteAux;
                VentasBrl.Fecha = venta.Fecha;
                VentasBrl.numeroVenta = venta.NumeroVenta;
                btnEditar.IsEnabled = true;
                btnEliminar.IsEnabled = true;
            }
            else
            {
                btnEliminar.IsEnabled = false;
                btnEditar.IsEnabled = false;
            }
        }
    }
}
