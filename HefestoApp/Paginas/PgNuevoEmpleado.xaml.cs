﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Comun;
using BRL;

namespace PresentacionWPF.Paginas
{
    /// <summary>
    /// Lógica de interacción para PgNuevoEmpleado.xaml
    /// </summary>
    public partial class PgNuevoEmpleado : Page
    {
        Empleado empleado;
        bool validado;
        public PgNuevoEmpleado()
        {
            InitializeComponent();
        }

        private void btnAtrasMenu_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PgEmpleados());
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (txtNombre.Text != "" && txtPrimerApellido.Text != "" && txtEmail.Text != "" && txtUsuario.Text !="" && txtContrasena.Text!="" && cbxCargoEmpleado.Text!="")
                validado = true;
            else
                validado = false;

            if (validado)
            {
                Usuario us = new Usuario();
                //us.IdUsuario = 0;
                //us.NombreUsuario = txtUsuario.Text;
                //us.Contrasena = txtContrasena.Text;
                //empleado = new Empleado()
                //{
                //    IdPersona = 0,
                //    Nombre = txtNombre.Text,
                //    PrimerApellido = txtPrimerApellido.Text,
                //    SegundoApellido = txtSegundoApellido.Text,
                //    Cargo = cbxCargoEmpleado.Text,
                //    Email = txtEmail.Text,
                //    Usuario = us,
                //};
                //empleado.Usuario.NombreUsuario =txtUsuario.Text;
                //empleado.Usuario.Contrasena = txtContrasena.Text;
                EmpleadoBrl.Insert(empleado);
                CuadroMensaje.Visibility = Visibility.Visible;
                CuadroMensaje.Background = new SolidColorBrush(Colors.Green);
                Mensaje.Text = "DATOS INGRESADOS CORRECTAMENTE";

                NavigationService.Navigate(new PgEmpleados());
            }
            else
            {
                CuadroMensaje.Background = new SolidColorBrush(Colors.Red);
                CuadroMensaje.Visibility = Visibility.Visible;
                Mensaje.Text = "ERROR AL INGRESAR LOS DATOS";
            }
        }
    }
}
