﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BRL;
using Comun;

namespace PresentacionWPF.Paginas
{
    /// <summary>
    /// Lógica de interacción para PgClientes.xaml
    /// </summary>
    public partial class PgClientes : Page
    {
        public PgClientes()
        {
            InitializeComponent();
            CargarDatos();
        }

        private void btnNuevoCliente_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PgNuevoCliente());
        }

        private void btnEditarCliente_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PgEditarClientes());
        }

        private void btnEliminarCliente_Click(object sender, RoutedEventArgs e)
        {
            if (dtgClientes.SelectedIndex != -1)
            {
                Ventanas.WinEliminarCliente ventanaEliminar = new Ventanas.WinEliminarCliente();
                ventanaEliminar.ShowDialog();
                if (ventanaEliminar.DialogResult.Value)
                {

                }
                NavigationService.Navigate(new PgClientes());
            }
        }

        private void dtgClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtgClientes.SelectedIndex != -1)
            {
                ClienteBrl.clienteSeleccionado = this.dtgClientes.SelectedItem as Cliente;
                btnEditarCliente.IsEnabled = true;
                btnEliminarCliente.IsEnabled = true;
            }
            else
            {
                btnEliminarCliente.IsEnabled = false;
                btnEditarCliente.IsEnabled = false;
            }
        }

        private void CargarDatos()
        {
            List<Cliente> clientes = new List<Cliente>();
            clientes.Clear();
            clientes = ClienteBrl.GetClientes();
            this.dtgClientes.ItemsSource = null;
            this.dtgClientes.ItemsSource = clientes;
        }

    }
}
