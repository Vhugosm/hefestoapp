﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BRL;
using Comun;

namespace PresentacionWPF.Paginas
{
    /// <summary>
    /// Lógica de interacción para PgNuevoCliente.xaml
    /// </summary>
    public partial class PgNuevoCliente : Page
    {
        bool validado;
        Cliente cliente;
        public PgNuevoCliente()
        {
            InitializeComponent();
        }

        private void btnNuevoCliente_Click(object sender, RoutedEventArgs e)
        {
            btnCancelar.IsEnabled = true;
            txtNombreCliente.IsEnabled = true;
            txtPrimerApellido.IsEnabled = true;
            txtSegundoApellido.IsEnabled = true;
            txtEmailCliente.IsEnabled = true;
            txtOrganizacionCliente.IsEnabled = true;
            btnGuardarCliente.IsEnabled = true;
            btnNuevoCliente.IsEnabled = false;
        }

        private void btnAtrasMenu_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PgClientes());
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            txtNombreCliente.Text = "";
            txtPrimerApellido.Text = "";
            txtSegundoApellido.Text = "";
            txtEmailCliente.Text = "";
            txtOrganizacionCliente.Text = "";
            btnCancelar.IsEnabled = false;
            txtNombreCliente.IsEnabled = false;
            txtPrimerApellido.IsEnabled = false;
            txtSegundoApellido.IsEnabled = false;
            txtEmailCliente.IsEnabled = false;
            txtOrganizacionCliente.IsEnabled = false;
            btnGuardarCliente.IsEnabled = false;
            btnNuevoCliente.IsEnabled = true;
        }

        private void btnGuardarCliente_Click(object sender, RoutedEventArgs e)
        {
            if (txtNombreCliente.Text != "" && txtPrimerApellido.Text != "")
                validado = true;
            else
                validado = false;

            if (validado)
            {
                //cliente = new Cliente()
                //{
                //    IdPersona = new Guid(),
                //    Nombre = txtNombreCliente.Text,
                //    PrimerApellido = txtPrimerApellido.Text,
                //    SegundoApellido = txtSegundoApellido.Text,
                //    Email = txtEmailCliente.Text,
                //    Organizacion = txtOrganizacionCliente.Text
                //};
                //cliente = new Cliente();
                //cliente.IdCliente = 0;
                //cliente.Nombre = txtNombreCliente.Text;
                //cliente.PrimerApellido = txtPrimerApellido.Text;
                //cliente.SegundoApellido = txtSegundoApellido.Text;
                //cliente.Email = txtEmailCliente.Text;
                //cliente.Organizacion = txtOrganizacionCliente.Text;
                //ClienteBrl.Insertar(cliente);
                CuadroMensaje.Visibility = Visibility.Visible;
                CuadroMensaje.Background = new SolidColorBrush(Colors.Green);
                Mensaje.Text = "DATOS INGRESADOS CORRECTAMENTE";
                txtNombreCliente.Text = "";
                txtPrimerApellido.Text = "";
                txtSegundoApellido.Text = "";
                txtEmailCliente.Text = "";
                txtOrganizacionCliente.Text = "";
                NavigationService.Navigate(new PgClientes());
            }
            else
            {
                CuadroMensaje.Background = new SolidColorBrush(Colors.Red);
                CuadroMensaje.Visibility = Visibility.Visible;
                Mensaje.Text = "ERROR AL INGRESAR LOS DATOS";
            }
        }
    }
}
