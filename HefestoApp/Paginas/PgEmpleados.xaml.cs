﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BRL;
using Comun;

namespace PresentacionWPF.Paginas
{
    /// <summary>
    /// Lógica de interacción para PgEmpleados.xaml
    /// </summary>
    public partial class PgEmpleados : Page
    {
        public PgEmpleados()
        {
            InitializeComponent();
            CargarDatos();
        }

        private void CargarDatos()
        {
            List<Empleado> empleados = new List<Empleado>();
            empleados.Clear();
            //empleados = EmpleadoBrl.GetEmpleados();
            this.dtgEmpleados.ItemsSource = null;
            this.dtgEmpleados.ItemsSource = empleados;
        }

        private void dtgEmpleados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtgEmpleados.SelectedIndex != -1)
            {
                EmpleadoBrl.empleadoSeleccionado = this.dtgEmpleados.SelectedItem as Empleado;
                btnEditarEmpleado.IsEnabled = true;
                btnEliminarEmpleado.IsEnabled = true;
            }
            else
            {
                btnEliminarEmpleado.IsEnabled = false;
                btnEditarEmpleado.IsEnabled = false;
            }
        }

        private void btnNuevoEmpleado_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PgNuevoEmpleado());
        }

        private void btnEditarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PgEditarEmpleado());
        }
    }
}
