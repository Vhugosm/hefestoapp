﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BRL;
using Comun;

namespace PresentacionWPF.Ventanas
{
    /// <summary>
    /// Lógica de interacción para WinEliminarCliente.xaml
    /// </summary>
    public partial class WinEliminarCliente : Window
    {
        public WinEliminarCliente()
        {
            InitializeComponent();
            CargarMensaje();
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void CargarMensaje()
        {
            Mensaje.Text = "¿Seguro de Eliminar al Cliente: " + ClienteBrl.clienteSeleccionado.RazonSocial + "?";
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            //ClienteBrl.Eliminar(ClienteBrl.clienteSeleccionado.IdCliente);
            this.Close();
        }
    }
}
