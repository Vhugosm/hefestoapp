﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Comun;

namespace PresentacionWPF
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //ouble alto = 0, ancho = 0;
        public MainWindow()
        {
            InitializeComponent();
            obtenerTamaño();
        }

        private void GridBarraTitulo_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void btnProductos_Click(object sender, RoutedEventArgs e)
        {
            Contenido.Source = new Uri("Paginas/PgProductos.xaml",UriKind.RelativeOrAbsolute);
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void obtenermedidas_Click(object sender, RoutedEventArgs e)
        {
        }

        private void btnMinimizar_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btnClientes_Click(object sender, RoutedEventArgs e)
        {
            Contenido.Source = new Uri("Paginas/PgClientes.xaml", UriKind.RelativeOrAbsolute);
        }

        private void btnEmpleados_Click(object sender, RoutedEventArgs e)
        {
            Contenido.Source = new Uri("Paginas/PgEmpleados.xaml", UriKind.RelativeOrAbsolute);
        }

        private void btnMenu_Click(object sender, RoutedEventArgs e)
        {
            GridMenu.Visibility = Visibility.Visible;
            btnMenu.Visibility = Visibility.Hidden;
            btnMenuBack.Visibility = Visibility.Visible;
            
        }

        private void btnMenuBack_Click(object sender, RoutedEventArgs e)
        {
            GridMenu.Visibility = Visibility.Collapsed;
            btnMenu.Visibility = Visibility.Visible;
            btnMenuBack.Visibility = Visibility.Hidden;
        }

        private void btnAlmacen_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnCerrarSesion_Click(object sender, RoutedEventArgs e)
        {
            //metodo de logout
            Login log = new Login();
            log.Show();
            this.Close();
        }

        public void obtenerTamaño()
        {
            Operaciones.alto = Contenido.Height;
            Operaciones.ancho = Contenido.Width;
        }

        private void BtnVentas_Click(object sender, RoutedEventArgs e)
        {
            Contenido.Source = new Uri("Paginas/PgVentas.xaml", UriKind.RelativeOrAbsolute);
        }
    }
}
