﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Comun;
using DAL;

namespace BRL
{
    public class ProductoBrl
    {
        public static Producto productoSeleccionado = new Producto();
        public static Producto productoVenta = new Producto();
        static Producto producto = null;
        static List<Producto> productos = null;

        public static Producto GetProducto(string id)
        {
            try
            {producto = DAL.ProductoDal.GetProducto(id);}
            catch (SqlException ex)
            {throw ex;}
            catch (Exception ex)
            {throw ex;}
            return producto;
        }

        public static List<Producto> GetProductos()
        {
            try
            {
                productos = DAL.ProductoDal.GetProductos();
            }
            catch (SqlException ex)
            {
                
                throw ex;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

            

            return productos;
        }

        public static void AgregarProducto(Producto Producto)
        {
            try
            {
                DAL.ProductoDal.Insertar(Producto);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        public static void ActualizarProducto(Producto producto)
        {
            try
            {
                DAL.ProductoDal.Actualizar(producto);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "ActualizarProducto", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "ActualizarProducto", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            Operaciones.WriteLogsDebug("ProductoDal", "ActualizarProducto", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Termino de ejecutar  el método lógica de negocio para Actualizar un Producto"));
        }

        public static void EliminarProducto(string id)
        {
            try
            {
                DAL.ProductoDal.Eliminar(id);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "ElimiarProducto", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "ElimiarProducto", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            Operaciones.WriteLogsDebug("ProductoDal", "ElimiarProducto", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Termino de ejecutar  el método lógica de negocio para Eliminar un Producto"));
        }
    }
}
