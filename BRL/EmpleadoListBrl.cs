﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Comun;
using DAL;

namespace BRL
{
    /// <summary>
    /// Clase que sirve la logica de negocio de lista de empleados
    /// </summary>
    public class EmpleadoListBrl
    {
        /// <summary>
        /// Metodo logica de negocio para insertar una lista de empleados para el reporte
        /// </summary>  
        public static DAL.PersonaDAL.EmpleadoDataSet ObtenerListaEmpleadoReporte()
        {
            Operaciones.WriteLogsDebug("EmpleadoListBrl", "ObtenerListaEmpleadoReporte", string.Format("{0} Info: {1}",DateTime.Now.ToString(),
                "Empezando a ejecutar el método lógica de negocio para Obtener Lista Empleados Reporte"));

            DAL.PersonaDAL.EmpleadoDataSet empleadoDatSet = null;
            try
            {
                empleadoDatSet = EmpleadoListDal.ObtenerListaEmpleadoReporte();
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("EmpleadoListBrl", "ObtenerListaEmpleadoReporte", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("EmpleadoListBrl", "ObtenerListaEmpleadoReporte", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("EmpleadoListBrl", "ObtenerListaEmpleadoReporte", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para Obtener Lista Empleados Reporte"));

            return empleadoDatSet;
        }
    }
}
