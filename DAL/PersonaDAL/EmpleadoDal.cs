﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using Comun;

namespace DAL
{
    public static class EmpleadoDal
    {
        /// <summary>
        /// Selecciona las personas
        /// </summary>
        /// <returns></returns>
        public static DataTable Select()
        {
            SqlCommand cmd = null;
            DataTable res = new DataTable();
            string query = @"SELECT IdPersona,Cargo,Eliminado FROM Empleado WHERE Eliminado=0";
            string query1 = "SELECT IDPERSONA, P.NOMBRES AS \"Nombres\", P.PRIMERAPELLIDO AS \"Primer apellido\", P.SEGUNDOAPELLIDO AS \"Segundo apellido\", E.CI AS \"CI\", E.CARGO AS \"Cargo\" FROM EMPLEADO E INNER JOIN PERSONA P ON P.IDPERSONA = E.IDEMPLEADO WHERE P.ESTADO = 1 ";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                res = OperacionesSql.ExcecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }

        /// <summary>
        /// Inserta una Empleado a la base de datos 
        /// </summary>
        /// <param name="emp"></param>
        public static void Insert(Empleado emp)
        {
            Operaciones.WriteLogsDebug("EmpleadoDal", "Insertar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear un empleado"));
            SqlCommand command = null;

            //Consulta para insertar empleados
            string query1 = "INSERT INTO Persona(idPersona, nombre, primerApellido, segundoApellido, idUsuario) VALUES (@idPersona, @nombres, @primerApellido, @segundoApellido, @idUsuario)";
            string query2 = "INSERT INTO EMPLEADO (idEmpleado, ci, cargo) VALUES (@IDEMPLEADO, @CI, @CARGO)";

            List<SqlCommand> cmds = new List<SqlCommand>();
            //Declaro e inicio la conexion
            SqlConnection conexion = OperacionesSql.ObtenerConexion();

            //Declaro la transaccion
            SqlTransaction transaccion = null;
            try
            {
                //Abro la conexion a la base de datos
                conexion.Open();


                //Inserto al empleado
                cmds = OperacionesSql.CreateNBasicCommand(2);
                SqlCommand cmd1 = cmds[0];
                SqlCommand cmd2 = cmds[1];

                cmd1.CommandText = query1;
                cmd2.CommandText = query2;

                int idPersona = OperacionesSql.GetIDGenerateTable("Persona");

                cmd1.Parameters.AddWithValue("idPersona", idPersona);
                cmd1.Parameters.AddWithValue("nombre", emp.Nombre);
                cmd1.Parameters.AddWithValue("primerApellido", emp.PrimerApellido);
                cmd1.Parameters.AddWithValue("segundoApellido", emp.SegundoApellido);
                cmd1.Parameters.AddWithValue("idUsuario", Sesion.idSesion);

                cmd2.Parameters.AddWithValue("IDEMPLEADO", idPersona);
                cmd2.Parameters.AddWithValue("CI", emp.Ci);
                cmd2.Parameters.AddWithValue("CARGO", emp.Cargo);

                OperacionesSql.Execute2BasicCommand(cmd1, cmd2);
                Logs.escribirAccion("INSERT", "Persona, EMPLEADO", true);

            }
            catch (Exception ex)
            {
                //Ojo escribir en el Log
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
        }

        /// <summary>
        /// Actualiza un empleado
        /// </summary>
        /// <param name="emp"></param>
        public static void Update(Empleado emp)
        {
            Operaciones.WriteLogsDebug("EmpleadoDal", "Actualizar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para Actualizar un Empleado"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string query1 = "UPDATE Persona SET nombre = @nombre, primerApellido = @primerApellido, segundoApellido = @segundoApellido, idUsuario = @idUsuario WHERE idPersona = @idPersona";
            string query2 = "UPDATE EMPLEADO SET CI = @CI, CARGO = @CARGO WHERE IDEMPLEADO = @IDEMPLEADO";
            List<SqlCommand> cmds = new List<SqlCommand>();
            try
            {

                cmds = OperacionesSql.CreateNBasicCommand(2);

                SqlCommand cmd1 = cmds[0];
                SqlCommand cmd2 = cmds[1];

                cmd1.CommandText = query1;
                cmd2.CommandText = query2;

                cmd1.Parameters.AddWithValue("nombres", emp.Nombre);
                cmd1.Parameters.AddWithValue("primerApellido", emp.PrimerApellido);
                cmd1.Parameters.AddWithValue("segundoApellido", emp.SegundoApellido);
                cmd1.Parameters.AddWithValue("idUsuario", Sesion.idSesion);
                cmd1.Parameters.AddWithValue("idPersona", emp.IdPersona);

                cmd2.Parameters.AddWithValue("CI", emp.Ci);
                cmd2.Parameters.AddWithValue("CARGO", emp.Cargo); ;
                cmd2.Parameters.AddWithValue("IDEMPLEADO", emp.IdPersona);

                OperacionesSql.Execute2BasicCommand(cmd1, cmd2);
                Logs.escribirAccion("UPDATE", "Persona, EMPLEADO", true);
            }
            catch (Exception ex)
            {
                Logs.registrarError("UPDATE", "Persona, EMPLEADO", ex.Message, true);
                throw ex;
            }
        }

        /// <summary>
        /// Realiza un eliminado logico de un empleado
        /// </summary>
        /// <param name="idEmpleado"></param>
        public static void Delete(int idEmpleado)
        {
            Operaciones.WriteLogsDebug("EmpleadoDal", "Eliminar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Cliente"));

            SqlCommand cmd = null;

            // Proporcionar la cadena de consulta 
            string query = @"UPDATE persona set estado = 0
                                    WHERE IdPersona=@idPersona";
            //Declaro e inicio la conexion
            SqlConnection conexion = OperacionesSql.ObtenerConexion();

            //Declaro la transaccion
            SqlTransaction transaccion = null;
            try
            {
                //Abro la conexion a la base de datos
                conexion.Open();



                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idPersona", idEmpleado);
                OperacionesSql.ExecuteBasicCommand(cmd);
                Logs.escribirAccion("DELETE", "Persona, EMPLEADO");
            }

            catch (Exception ex)
            {
                Logs.registrarError("DELETE", "Persona, EMPLEADO", ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Recupera un empleado con su codigo
        /// </summary>
        /// <param name="idEmpleado"></param>
        public static Empleado Get(int idEmpleado)
        {
            Empleado res = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT IDPERSONA, 
                            P.NOMBRE, 
                            P.PRIMERAPELLIDO, 
                            P.SEGUNDOAPELLIDO, 
                            E.CI, 
                            E.CARGO 
                            FROM EMPLEADO E 
                            INNER JOIN PERSONA P ON P.IDPERSONA = E.IDEMPLEADO 
                            WHERE P.ESTADO = 1 AND P.IDPERSONA = @IDEMPLEADO";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("IDEMPLEADO", idEmpleado);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new Empleado();
                    res.IdPersona = int.Parse(dr[0].ToString());
                    res.Nombre = (dr[1].ToString());
                    res.PrimerApellido = (dr[2].ToString());
                    res.SegundoApellido = (dr[3].ToString());
                    res.Ci = (dr[4].ToString());
                    res.Cargo = (dr[5].ToString());

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }
    }
}
