﻿using System;
using System.Data.SqlClient;
using Comun;
namespace DAL
{
    /// <summary>
    /// Clase que sirve para manipular una lista de empleados
    /// </summary>
    public static class EmpleadoListDal
    {
        /// <summary>
        /// Metodo que sirve para obtener una lista de empleados para el reporte
        /// </summary>  
        public static DAL.PersonaDAL.EmpleadoDataSet ObtenerListaEmpleadoReporte()
        {
            Operaciones.WriteLogsDebug("EmpleadoListDal", "ObtenerListaEmpleadoReporte",
                string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para obtener una lista de empleados"));

            PersonaDAL.EmpleadoDataSet empleadoDataSet = new PersonaDAL.EmpleadoDataSet();
            //EmpleadoListaReporteTableAdapter empleadoTableAdapter = new EmpleadoListaReporteTableAdapter();
            try
            {
                //empleadoTableAdapter.Fill(empleadoDataSet.Tables["EmpleadoListaReporte"] as DAL.PersonaDAL.EmpleadoDataSet.EmpleadoListaReporteDataTable);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("EmpleadoListDal", "ObtenerListaEmpleadoReporte", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("EmpleadoListDal", "ObtenerListaEmpleadoReporte", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            Operaciones.WriteLogsDebug("EmpleadoListDal", "ObtenerListaEmpleadoReporte", string.Format("{0}  Info: {1}", DateTime.Now.ToString(),
                "Termino de ejecutar  el metodo acceso a datos para  obtener una lista de empleados"));

            return empleadoDataSet;
        }
    }
}
