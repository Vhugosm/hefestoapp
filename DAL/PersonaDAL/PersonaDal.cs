﻿using System;
using System.Data;
using System.Data.SqlClient;
using Comun;

namespace DAL
{
    /// <summary>
    /// Clase persona correspondiente a la capa de datos
    /// </summary>
    public class PersonaDal : AbstractDAL
    {

        #region Atributos, Propiedades y Constructores
        private Persona persona;

        public Persona Persona
        {
            get
            {
                return persona;
            }

            set
            {
                persona = value;
            }
        }


        public PersonaDal()
        {

        }
        public PersonaDal(Persona persona)
        {
            this.persona = persona;
        }
        #endregion

        /// <summary>
        /// Elimina Persona de la base de datos
        /// </summary>
        /// <param name="idPersona"></param>
        public override void Delete()
        {
            Operaciones.WriteLogsDebug("PersonaDal", "Eliminar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para Eliminar una Persona"));
            SqlCommand cmd = null;
            // Proporcionar la cadena de consulta 
            string query = @"UPDATE Persona SET estado=0, idUsuario = @idUsuario
                                    WHERE idPersona = @idPersona";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("idUsuario", Sesion.idSesion);
                cmd.Parameters.AddWithValue("idPersona", Persona.IdPersona);
                OperacionesSql.ExecuteBasicCommand(cmd);
                Logs.escribirAccion("UPDATE", "Persona");
            }
            catch (Exception ex)
            {
                Logs.registrarError("UPDATE", "Persona", ex.Message);
                throw ex;
            }

        }

        /// <summary>
        /// Inserta una Persona
        /// </summary>
        public override void Insert()
        {
            Operaciones.WriteLogsDebug("PersonaDal", "Insertar", string.Format("{0} Info: {1}",
           DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear una persona"));

            SqlCommand cmd = null;

            //Consulta para insertar personas
            string query = @"INSERT INTO Persona(nombre, primerApellido, segundoApellido, idUsuario) 
                                    VALUES(@nombre, @primerApellido, @segundoApellido, @idUsuario)";
            //Declaro e inicio la conexion
            SqlConnection conexion = OperacionesSql.ObtenerConexion();

            //Declaro la transaccion
            SqlTransaction transaccion = null;
            try
            {
                //Abro la conexion a la base de datos
                conexion.Open();

                //Inicio la transaccion
                transaccion = conexion.BeginTransaction();

                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("nombre", persona.Nombre);
                cmd.Parameters.AddWithValue("primerApellido", persona.PrimerApellido);
                cmd.Parameters.AddWithValue("segundoApellido", persona.SegundoApellido);
                cmd.Parameters.AddWithValue("idUsuario", Sesion.idSesion);
                OperacionesSql.ExecuteBasicCommand(cmd);
                Logs.escribirAccion("INSERT", "Persona");

            }
            catch (Exception ex)
            {
                Logs.registrarError("INSERT", "Persona", ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Selecciona las personas
        /// </summary>
        /// <returns></returns>
        public override DataTable Select()
        {
            SqlCommand cmd = null;
            DataTable res = new DataTable();
            string query = @"SELECT * FROM vwPersonaSelect ORDER BY 2";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                res = OperacionesSql.ExcecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }

        /// <summary>
        /// Actualiza las personas
        /// </summary>
        /// <param name="Persona"></param>
        public override void Update()
        {
            Operaciones.WriteLogsDebug("PersonaDal", "Actualizar", string.Format("{0} Info: {1}", DateTime.Now.ToLongDateString(), "Empezando a ejecutar el metodo acceso a datos para Actualizar una Persona"));
            SqlCommand cmd = null;
            // Proporcionar la cadena de consulta 
            string query = @"UPDATE Persona SET nombre=@nombre, primerApellido=@primerApellido, segundoApellido=@segundoApellido,idUsuario = @idUsuario 
                                    WHERE idPersona=@idPersona";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombre", persona.Nombre);
                cmd.Parameters.AddWithValue("@primerApellido", persona.PrimerApellido);
                cmd.Parameters.AddWithValue("@segundoApellido", persona.SegundoApellido);
                cmd.Parameters.AddWithValue("idUsuario", Sesion.idSesion);
                cmd.Parameters.AddWithValue("idPersona", persona.IdPersona);
                OperacionesSql.ExecuteBasicCommand(cmd);
                Logs.escribirAccion("Update", "Persona");

            }
            catch (Exception ex)
            {
                Logs.registrarError("Update", "Persona", ex.Message);
                throw ex;
            }

        }

        /// <summary>
        /// Obtiene una persona a partir de su id
        /// </summary>
        /// <param name="idPersona></param>
        /// <returns></returns>
        public Persona Get(int idPersona)
        {
            Persona res = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT idPersona, nombre, primerApellido, segundoApellido, estado FROM Persona WHERE IdPersona=@idPersona and Eliminado=0";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", idPersona);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    res = new Persona(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), byte.Parse(dr[4].ToString()));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }
    }
}
