﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos Producto
    /// </summary>
    public class Producto
    {
         #region propiedades

        public string IdProducto { get; set; }
        public string Descripcion { get; set; }
        public string UnidadVenta { get; set; }
        public decimal Precio { get; set; }
        public int Cantidad { get; set; }
        public byte Estado { get; set; }

        #endregion
        public Producto() { }
    }
}
