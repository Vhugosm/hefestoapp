﻿using System;
using System.Collections.Generic;


namespace Comun
{
    /// <summary>
    /// Clase padre que sirve para crear personas
    /// </summary>
    public class Persona
    {
        #region Atributos
        private int idPersona;
        private string nombre;
        private string primerApellido;
        private string segundoApellido;
        private byte estado;
        #endregion

        #region propiedades

        public int IdPersona
        {
            get
            {
                return idPersona;
            }

            set
            {
                idPersona = value;
            }
        }
        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }
        public string PrimerApellido
        {
            get
            {
                return primerApellido;
            }

            set
            {
                primerApellido = value;
            }
        }
        public string SegundoApellido
        {
            get
            {
                return segundoApellido;
            }

            set
            {
                segundoApellido = value;
            }
        }
        public byte Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        #endregion

        #region Constructores
        public Persona()
        {

        }
        public Persona(int idPersona, string nombres, string primerApellido, string segundoApellido, byte estado)
        {
            this.IdPersona = idPersona;
            this.Nombre = nombres;
            this.PrimerApellido = primerApellido;
            this.SegundoApellido = segundoApellido;
            this.Estado = estado;
        }

        public Persona(string nombres, string primerApellido, string segundoApellido)
        {
            this.Nombre = nombres;
            this.PrimerApellido = primerApellido;
            this.SegundoApellido = segundoApellido;
        }
        #endregion
    }
}
