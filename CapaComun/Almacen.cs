﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos Almacen
    /// </summary>
    public class Almacen
    {
        
        #region propiedades

        public int IdAlmacen { get; set; }
        public string Descripcion { get; set; }
        public string Direccion { get; set; }
        public bool Estado { get; set; }
        #endregion
        public Almacen() { }
       
    }
}
