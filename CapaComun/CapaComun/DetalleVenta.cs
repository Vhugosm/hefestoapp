﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos DetalleVenta
    /// </summary>
    public class DetalleVenta
    {
        #region propiedades

        /// <summary>
        /// Identificador del detalle de la venta
        /// </summary>
        public int IdDetalleVenta { get; set; }

        /// <summary>
        /// Informacion de la venta realizada
        /// </summary>
        public Venta Venta { get; set; }

        /// <summary>
        /// Informacion del producto vendido
        /// </summary>
        public Producto Producto { get; set; }

        /// <summary>
        /// Cantidad de productos vendido
        /// </summary>
        public int CantidadProducto { get; set; }

        #endregion
        public DetalleVenta() { }
    }
}
