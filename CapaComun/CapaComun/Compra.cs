﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos Compra
    /// </summary>
    public class Compra
    {
        #region propiedades

        /// <summary>
        /// 
        /// </summary>
        public int IdCompra { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public Proveedor Proveedor { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Empleado Empleado { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Fecha { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int TotalCompra { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public byte Estado { get; set; }

        #endregion
        public Compra() { }
    }
}
