﻿using System;

namespace Univalle.Fie.Sistemas.BaseDeDatos2.SevpaApp.Comun
{
    public class TipoUsuario
    {
        #region Atributos

        private byte idTipoUsuario;
        private string descripcion;

        #endregion

        #region propiedades

        public byte IdTipoUsuario { get; set; }
        public string Descripcion { get; set; }

        #endregion

        #region Constructor

        public TipoUsuario(byte idTipoUsuario, string descripcion)
        {
            this.idTipoUsuario = idTipoUsuario;
            this.descripcion = descripcion;
        }

        #endregion
    }
}
