﻿using System;

namespace Comun
{
    public class ImagenProducto
    {
        #region propiedades

        public int IdImagen { get; set; }
        public Producto Producto { get; set; }
        public string Direccion { get; set; }

        #endregion
        public ImagenProducto() { }
    }
}
